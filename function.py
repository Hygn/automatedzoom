def openZoom(rid,pwd):
    import subprocess
    import os
    appdata = os.environ['APPDATA']
    confurl = f'"zoommtg://zoom.us/join?action=join&confno={rid}&pwd={pwd}"'
    subprocess.run(f"{appdata}/Zoom/bin/Zoom.exe --url={confurl}", shell=True)
def extZoom(link=''):
    rid = link.split('zoom.us/j/')[1].split('?pwd=')[0]
    pwd = link.split('?pwd=')[1]
    if '#' in pwd:
        pwd = pwd.split('#')[0]
    return [rid,pwd]