import os
import datetime
import function
import time
import sys
from win10toast import ToastNotifier
toast = ToastNotifier()
try:
    timecfg = open(os.getcwd()+'/time.cfg','r').read()
except FileNotFoundError:
    print('[ERROR] time.csv 파일을 읽을 수 없습니다.')
    exit()
try:
    timetable = open(os.getcwd()+'/timetable.csv','r').read()
except FileNotFoundError:
    print('[ERROR] timetable.csv 파일을 읽을 수 없습니다.')
    exit()
try:
    timedict = {}
    timecfg = timecfg.split('\n')
    for i in timecfg:
        int(i.split('=')[0])
        timedict.update({i.split('=')[0]:i.split('=')[1]})
except:
    print('[ERROR] time.cfg 파일을 파싱할 수 없습니다.')
    exit()
try:
    currentDay = None
    timetableDict = {}
    timetable = timetable.split('\n')
    for i in timetable:
        try:
            cl = i.split(',')[0].strip()
            try:
                zl = i.split(',')[1].strip()
                cln = i.split(',')[2].strip()
                int(cl)
            except:
                raise TypeError
            if currentDay == None:
               raise Exception
            else:
                timetableDict[currentDay].append({cl:[zl,cln]})
        except TypeError:
            if cl in ['mon','tue','wed','thu','fri','sat','sun']:
                try:
                    timetableDict[cl]
                except KeyError:
                    timetableDict.update({cl:[]})
                currentDay = cl
            elif cl != '':
                raise Exception
except:
    print('[ERROR] timetable.csv 파일을 파싱할 수 없습니다.')
    exit()
try:
    try:
        startday = sys.argv[2]
        if startday not in ['mon','tue','wed','thu','fri','sat','sun']:
            raise IndexError
        timetableDict = timetableDict[startday]
    except IndexError:
        timetableDict = timetableDict[datetime.datetime.now().strftime('%a').lower()]
    t_timetableDict = timetableDict
    try:
        startat = int(sys.argv[1])
    except (TypeError,IndexError):
        startat = -1
    for i in timetableDict:
        runTime = timedict[list(i.keys())[0]]
        if startat <= int(list(i.keys())[0]):
            print(f"({list(i.keys())[0]} 교시) {runTime} 에 예약된 {i[list(i.keys())[0]][0]}({i[list(i.keys())[0]][1]}) 강의를 대기중입니다")
            toast.show_toast('ZoomBOT',f"{runTime} 에 예약된 {i[list(i.keys())[0]][0]}({i[list(i.keys())[0]][1]}) 강의를 대기중입니다", duration=3,threaded=True)
        while startat <= int(list(i.keys())[0]):
            time.sleep(1)
            if datetime.datetime.now().strftime('%H:%M') == runTime:
                zCfg = function.extZoom(i[list(i.keys())[0]][0])
                print(f"{i[list(i.keys())[0]][1]} 강의에 입장중입니다..")
                toast.show_toast('ZoomBOT',f"{i[list(i.keys())[0]][1]} 강의에 입장중입니다..", duration=3,threaded=True)
                function.openZoom(zCfg[0],zCfg[1])
                break
except KeyboardInterrupt:
    exit()
except:
    print('[ERROR] 내부 데이터 처리 오류가 발생했습니다.')
    exit()